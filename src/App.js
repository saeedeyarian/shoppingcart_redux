import './App.css'; 
import { Routes, Route } from 'react-router-dom';

//redux
import { Provider } from 'react-redux';
import store from './components/redux/store';

//componen
import Shop from './components/Shop';

function App() {
  return (
    <Provider store={store}>
      <Routes>
        <Route path='/' element={<Shop/>}/>
      </Routes>
    </Provider>
  );
}

export default App;
