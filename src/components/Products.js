import React from 'react';

const Products = ({productData}) => {
    return (
        <div>
            <img src={productData.image} alt="product"/>
            <p>{productData.title}</p>
        </div>
    );
};

export default Products;