import React, { useEffect } from 'react';

//redux
import { useDispatch, useSelector } from "react-redux";

//components
import { fetchProducts } from './redux/products/productAction';
import Products from "./Products";

const Shop = () => {

    const dispatch = useDispatch();
    const productState = useSelector( state => state.productState);

    useEffect( () => {
        dispatch(fetchProducts())
    }, []);

    return (
        <div>
            {productState.loading ?
                <h2>Loading...</h2> :
            productState.error ?
                <p>somthing went wrong</p> :
            productState.products.map(product =>
                <Products key={product.id} productData={product}/>)
            }
        </div>
    );
};

export default Shop;